package pe.orbis.mylibrary;

import android.view.View;

/**
 * Created by Ricardo Bravo on 27/06/17.
 */

public interface OnRangeSeekbarChangeListener {
    void valueChanged(View view, Number minValue, Number maxValue);
}

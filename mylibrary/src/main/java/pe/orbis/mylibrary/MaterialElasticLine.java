package pe.orbis.mylibrary;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Ricardo Bravo on 27/06/17.
 */

public class MaterialElasticLine extends View {

    private static final int INVALID_POINTER_ID = 255;
    private static final float NO_STEP = -1f;
    private static final float NO_FIXED_GAP = -1f;

    static final class DataType {
        static final int LONG = 0;
        static final int DOUBLE = 1;
        static final int INTEGER = 2;
        static final int FLOAT = 3;
        static final int SHORT = 4;
        static final int BYTE = 5;
    }

    private OnRangeSeekbarChangeListener onRangeSeekbarChangeListener;
    private float absoluteMinValue;
    private float absoluteMaxValue;
    private float absoluteMinStartValue;
    private float absoluteMaxStartValue;
    private float minValue;
    private float maxValue;
    private float minStartValue;
    private float maxStartValue;
    private float steps;
    private float gap;
    private float fixGap;
    private int mActivePointerId = INVALID_POINTER_ID;
    private int dataType;
    private float cornerRadius;
    private int lineColor;
    private int lineSelectedColor;
    private int leftThumbColor;
    private int rightThumbColor;
    private int leftThumbColorNormal;
    private int leftThumbColorPressed;
    private int rightThumbColorNormal;
    private int rightThumbColorPressed;
    private float barPadding;
    private float barHeight;
    private float thumbWidth;
    private float thumbDiameter;
    private float thumbHeight;
    private Drawable leftDrawable;
    private Drawable rightDrawable;
    private Drawable leftDrawablePressed;
    private Drawable rightDrawablePressed;
    private Bitmap leftThumb;
    private Bitmap leftThumbPressed;
    private Bitmap rightThumb;
    private Bitmap rightThumbPressed;
    private Thumb pressedThumb;
    private double normalizedMinValue = 0d;
    private double normalizedMaxValue = 100d;
    private RectF rectF;
    private Paint paintF;
    private RectF rectLeftThumb;
    private RectF rectRightThumb;
    private boolean mIsDragging;

    private enum Thumb{ MIN, MAX }

    public MaterialElasticLine(Context context) {
        this(context, null);
    }

    public MaterialElasticLine(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MaterialElasticLine(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if(isInEditMode()) return;

        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.CrystalRangeSeekbar);
        try{
            cornerRadius = array.getFloat(R.styleable.CrystalRangeSeekbar_corner_radius, 0f);
            minValue = array.getFloat(R.styleable.CrystalRangeSeekbar_min_value, 0f);
            maxValue = array.getFloat(R.styleable.CrystalRangeSeekbar_max_value, 100f);
            minStartValue = array.getFloat(R.styleable.CrystalRangeSeekbar_min_start_value, minValue);
            maxStartValue = array.getFloat(R.styleable.CrystalRangeSeekbar_max_start_value, maxValue);
            steps = array.getFloat(R.styleable.CrystalRangeSeekbar_steps, NO_STEP);
            gap = array.getFloat(R.styleable.CrystalRangeSeekbar_gap, 0f);
            fixGap = array.getFloat(R.styleable.CrystalRangeSeekbar_fix_gap, NO_FIXED_GAP);
            lineColor = array.getColor(R.styleable.CrystalRangeSeekbar_line_color, Color.GRAY);
            lineSelectedColor = array.getColor(R.styleable.CrystalRangeSeekbar_line_selected_color,
                    Color.BLACK);
            leftThumbColorNormal = array.getColor(R.styleable.CrystalRangeSeekbar_left_thumb_color,
                    Color.BLACK);
            rightThumbColorNormal = array.getColor(R.styleable.CrystalRangeSeekbar_right_thumb_color,
                    Color.BLACK);
            leftThumbColorPressed = array.getColor(R.styleable.CrystalRangeSeekbar_left_thumb_color_pressed,
                    Color.DKGRAY);
            rightThumbColorPressed = array.getColor(R.styleable.CrystalRangeSeekbar_right_thumb_color_pressed,
                    Color.DKGRAY);
            leftDrawable = array.getDrawable(R.styleable.CrystalRangeSeekbar_left_thumb_image);
            rightDrawable = array.getDrawable(R.styleable.CrystalRangeSeekbar_right_thumb_image);
            leftDrawablePressed = array.getDrawable(R.styleable.CrystalRangeSeekbar_left_thumb_image_pressed);
            rightDrawablePressed = array.getDrawable(R.styleable.CrystalRangeSeekbar_right_thumb_image_pressed);
            thumbDiameter = array.getDimensionPixelSize(R.styleable.CrystalRangeSeekbar_thumb_diameter,
                    getResources().getDimensionPixelSize(R.dimen.thumb_size));
            dataType = array.getInt(R.styleable.CrystalRangeSeekbar_data_type, DataType.INTEGER);
        }
        finally {
            array.recycle();
        }

        init();
    }

    private void init(){
        absoluteMinValue = minValue;
        absoluteMaxValue = maxValue;
        leftThumbColor = leftThumbColorNormal;
        rightThumbColor = rightThumbColorNormal;
        leftThumb = getBitmap(leftDrawable);
        rightThumb = getBitmap(rightDrawable);
        leftThumbPressed = getBitmap(leftDrawablePressed);
        rightThumbPressed = getBitmap(rightDrawablePressed);
        leftThumbPressed = (leftThumbPressed == null) ? leftThumb : leftThumbPressed;
        rightThumbPressed = (rightThumbPressed == null) ? rightThumb : rightThumbPressed;

        gap = Math.max(0, Math.min(gap, absoluteMaxValue - absoluteMinValue));
        gap = gap / (absoluteMaxValue - absoluteMinValue) * 100;
        if(fixGap != NO_FIXED_GAP){
            fixGap = Math.min(fixGap, absoluteMaxValue);
            fixGap = fixGap / (absoluteMaxValue - absoluteMinValue) * 100;
            addFixGap(true);
        }

        thumbWidth  = getThumbWidth();
        thumbHeight = getThumbHeight();

        barHeight = getBarHeight();
        barPadding = getBarPadding();
        paintF = new Paint(Paint.ANTI_ALIAS_FLAG);
        rectF = new RectF();
        rectLeftThumb = new RectF();
        rectRightThumb = new RectF();
        pressedThumb = null;
        setMinStartValue();
        setMaxStartValue();
        setWillNotDraw(false);
    }

    public void setCornerRadius(float cornerRadius){
        this.cornerRadius = cornerRadius;
    }

    public void setMinValue(float minValue){
        this.minValue = minValue;
        this.absoluteMinValue = minValue;
    }

    public void setMaxValue(float maxValue){
        this.maxValue = maxValue;
        this.absoluteMaxValue = maxValue;
    }

    public void setMinStartValue(float minStartValue){
        this.minStartValue = minStartValue;
        this.absoluteMinStartValue = minStartValue;
    }

    public void setMaxStartValue(float maxStartValue){
        this.maxStartValue = maxStartValue;
        this.absoluteMaxStartValue = maxStartValue;
    }

    public void setSteps(float steps){
        this.steps = steps;
    }

    public void setGap(float gap){
        this.gap = gap;
    }

    public void setFixGap(float fixGap){
        this.fixGap = fixGap;
    }

    public void setLineColor(int barColor){
        this.lineColor = barColor;
    }

    public void setLineSelectedColor(int barHighlightColor){
        this.lineSelectedColor = barHighlightColor;
    }

    public void setLeftThumbColor(int leftThumbColorNormal){
        this.leftThumbColorNormal = leftThumbColorNormal;
    }

    public void setLeftThumbHighlightColor(int leftThumbColorPressed){
        this.leftThumbColorPressed = leftThumbColorPressed;
    }

    public void setLeftThumbDrawable(int resId){
        setLeftThumbDrawable(ContextCompat.getDrawable(getContext(), resId));
    }

    public void setLeftThumbDrawable(Drawable drawable){
        setLeftThumbBitmap(getBitmap(drawable));
    }

    public void setLeftThumbBitmap(Bitmap bitmap){
        leftThumb = bitmap;
    }

    public void setThumbDiameter(float thumbDiameter){
        this.thumbDiameter = thumbDiameter;
    }

    public void setLeftThumbHighlightDrawable(int resId){
        setLeftThumbHighlightDrawable(ContextCompat.getDrawable(getContext(), resId));
    }

    public void setLeftThumbHighlightDrawable(Drawable drawable){
        setLeftThumbHighlightBitmap(getBitmap(drawable));
    }

    public void setLeftThumbHighlightBitmap(Bitmap bitmap){
        leftThumbPressed = bitmap;
    }

    public void setRightThumbColor(int rightThumbColorNormal){
        this.rightThumbColorNormal = rightThumbColorNormal;
    }

    public void setRightThumbHighlightColor(int rightThumbColorPressed){
        this.rightThumbColorPressed = rightThumbColorPressed;
    }

    public void setRightThumbDrawable(int resId){
        setRightThumbDrawable(ContextCompat.getDrawable(getContext(), resId));
    }

    public void setRightThumbDrawable(Drawable drawable){
        setRightThumbBitmap(getBitmap(drawable));
    }

    public void setRightThumbBitmap(Bitmap bitmap){
        rightThumb = bitmap;
    }

    public void setRightThumbHighlightDrawable(int resId){
        setRightThumbHighlightDrawable(ContextCompat.getDrawable(getContext(), resId));
    }

    public void setRightThumbHighlightDrawable(Drawable drawable){
        setRightThumbHighlightBitmap(getBitmap(drawable));
    }

    public void setRightThumbHighlightBitmap(Bitmap bitmap){
        rightThumbPressed = bitmap;
    }

    public void setDataType(int dataType){
        this.dataType = dataType;
    }

    public void setOnRangeSeekbarChangeListener(OnRangeSeekbarChangeListener onRangeSeekbarChangeListener){
        this.onRangeSeekbarChangeListener = onRangeSeekbarChangeListener;
        if(this.onRangeSeekbarChangeListener != null){
            this.onRangeSeekbarChangeListener.valueChanged(this, getSelectedMinValue(), getSelectedMaxValue());
        }
    }

    public Number getSelectedMinValue(){
        double nv = normalizedMinValue;
        if(steps > 0 && steps <= ((Math.abs(absoluteMaxValue)) / 2)){
            float stp = steps / (absoluteMaxValue - absoluteMinValue) * 100;
            double halfStep = stp / 2;
            double mod = nv % stp;
            if(mod > halfStep){
                nv = nv - mod;
                nv = nv + stp;
            }
            else{
                nv = nv - mod;
            }
        }
        else{
            if(steps != NO_STEP)
                throw new IllegalStateException("steps out of range " + steps);
        }

        return formatValue(normalizedToValue(nv));
    }

    public Number getSelectedMaxValue(){

        double nv = normalizedMaxValue;
        if(steps > 0 && steps <= (Math.abs(absoluteMaxValue) / 2)){
            float stp = steps / (absoluteMaxValue - absoluteMinValue) * 100;
            double halfStep = stp / 2;
            double mod = nv % stp;
            if(mod > halfStep){
                nv = nv - mod;
                nv = nv + stp;
            }
            else{
                nv = nv - mod;
            }
        }
        else{
            if(steps != NO_STEP)
                throw new IllegalStateException("steps out of range " + steps);
        }

        return formatValue(normalizedToValue(nv));
    }

    protected Thumb getPressedThumb(){
        return pressedThumb;
    }

    private float getThumbWidth(){
        return (leftThumb != null)  ? leftThumb.getWidth() : getThumbDiameter();
    }

    private float getThumbHeight(){
        return (leftThumb != null)  ? leftThumb.getHeight() : getThumbDiameter();
    }

    private float getThumbDiameter(){
        return (thumbDiameter > 0) ? thumbDiameter :  getResources().getDimension(R.dimen.thumb_size);
    }

    private float getBarHeight(){
        return (thumbHeight * 0.5f) * 0.3f;
    }

    private float getBarPadding(){
        return thumbWidth * 0.5f;
    }

    private Bitmap getBitmap(Drawable drawable){
        return (drawable != null) ? ((BitmapDrawable) drawable).getBitmap() : null;
    }

    protected RectF getLeftThumbRect(){
        return rectLeftThumb;
    }

    protected RectF getRightThumbRect(){
        return rectRightThumb;
    }

    private void setupBar(final Canvas canvas, final Paint paint, final RectF rect){
        rect.left   = barPadding;
        rect.top    = 0.5f * (getHeight() - barHeight);
        rect.right  = getWidth() - barPadding;
        rect.bottom = 0.5f * (getHeight() + barHeight);

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(lineColor);
        paint.setAntiAlias(true);
        drawBar(canvas, paint, rect);
    }

    private void drawBar(final Canvas canvas, final Paint paint, final RectF rect){
        canvas.drawRoundRect(rect, cornerRadius, cornerRadius, paint);
    }

    private void setupHighlightBar(final Canvas canvas, final Paint paint, final RectF rect){
        rect.left = normalizedToScreen(normalizedMinValue) + (getThumbWidth() / 2);
        rect.right = normalizedToScreen(normalizedMaxValue) + (getThumbWidth() / 2);
        paint.setColor(lineSelectedColor);
        drawHighlightBar(canvas, paint, rect);
    }

    private void drawHighlightBar(final Canvas canvas, final Paint paint, final RectF rect){
        canvas.drawRoundRect(rect, cornerRadius, cornerRadius, paint);
    }

    private void setupLeftThumb(final Canvas canvas, final Paint paint, final RectF rect){
        leftThumbColor = (Thumb.MIN.equals(pressedThumb)) ? leftThumbColorPressed : leftThumbColorNormal;
        paint.setColor(leftThumbColor);

        rectLeftThumb.left   = normalizedToScreen(normalizedMinValue);
        rectLeftThumb.right  = Math.min(rectLeftThumb.left + (getThumbWidth() / 2) + barPadding, getWidth());
        rectLeftThumb.top    = 0f;
        rectLeftThumb.bottom = thumbHeight;

        if(leftThumb != null){
            Bitmap lThumb = (Thumb.MIN.equals(pressedThumb)) ? leftThumbPressed : leftThumb;
            drawLeftThumbWithImage(canvas, paint, rectLeftThumb, lThumb);
        }
        else{
            drawLeftThumbWithColor(canvas, paint, rectLeftThumb);
        }
    }

    private void drawLeftThumbWithColor(final Canvas canvas, final Paint paint, final RectF rect){
        canvas.drawOval(rect, paint);
    }

    private void drawLeftThumbWithImage(final Canvas canvas, final Paint paint, final RectF rect, final Bitmap image){
        canvas.drawBitmap(image, rect.left, rect.top, paint);
    }

    private void setupRightThumb(final Canvas canvas, final Paint paint, final RectF rect){

        rightThumbColor = (Thumb.MAX.equals(pressedThumb)) ? rightThumbColorPressed : rightThumbColorNormal;
        paint.setColor(rightThumbColor);

        rectRightThumb.left = normalizedToScreen(normalizedMaxValue);
        rectRightThumb.right = Math.min(rectRightThumb.left + (getThumbWidth() / 2) + barPadding, getWidth());
        rectRightThumb.top = 0f;
        rectRightThumb.bottom = thumbHeight;

        if(rightThumb != null){
            Bitmap rThumb = (Thumb.MAX.equals(pressedThumb)) ? rightThumbPressed : rightThumb;
            drawRightThumbWithImage(canvas, paint, rectRightThumb, rThumb);
        }
        else{
            drawRightThumbWithColor(canvas, paint, rectRightThumb);
        }
    }

    private void drawRightThumbWithColor(final Canvas canvas, final Paint paint, final RectF rect){
        canvas.drawOval(rect, paint);
    }

    private void drawRightThumbWithImage(final Canvas canvas, final Paint paint, final RectF rect, final Bitmap image){
        canvas.drawBitmap(image, rect.left, rect.top, paint);
    }

    private void trackTouchEvent(MotionEvent event){
        final int pointerIndexEvent = event.findPointerIndex(mActivePointerId);
        try{
            final float x = event.getX(pointerIndexEvent);

            if (Thumb.MIN.equals(pressedThumb)) {
                setNormalizedMinValue(screenToNormalized(x));
            } else if (Thumb.MAX.equals(pressedThumb)) {
                setNormalizedMaxValue(screenToNormalized(x));
            }
        }
        catch (Exception ignored){}
    }

    private int getMeasureSpecWith(int widthMeasureSpec){
        int width = 200;
        if (MeasureSpec.UNSPECIFIED != MeasureSpec.getMode(widthMeasureSpec)) {
            width = MeasureSpec.getSize(widthMeasureSpec);
        }
        return width;
    }

    private int getMeasureSpecHeight(int heightMeasureSpec){
        int height = Math.round(thumbHeight);
        if (MeasureSpec.UNSPECIFIED != MeasureSpec.getMode(heightMeasureSpec)) {
            height = Math.min(height, MeasureSpec.getSize(heightMeasureSpec));
        }
        return height;
    }

    private void setMinStartValue(){
        if(minStartValue > minValue && minStartValue < maxValue){
            minStartValue = Math.min(minStartValue, absoluteMaxValue);
            minStartValue -= absoluteMinValue;
            minStartValue = minStartValue / (absoluteMaxValue - absoluteMinValue) * 100;
            setNormalizedMinValue(minStartValue);
        }
    }

    private void setMaxStartValue(){
        if(maxStartValue < absoluteMaxValue && maxStartValue > absoluteMinValue && maxStartValue > absoluteMinStartValue){
            maxStartValue = Math.max(absoluteMaxStartValue, absoluteMinValue);
            maxStartValue -= absoluteMinValue;
            maxStartValue = maxStartValue / (absoluteMaxValue - absoluteMinValue) * 100;
            setNormalizedMaxValue(maxStartValue);
        }
    }

    private Thumb evalPressedThumb(float touchX){
        Thumb result = null;

        boolean minThumbPressed = isInThumbRange(touchX, normalizedMinValue);
        boolean maxThumbPressed = isInThumbRange(touchX, normalizedMaxValue);
        if (minThumbPressed && maxThumbPressed) {
            result = (touchX / getWidth() > 0.5f) ? Thumb.MIN : Thumb.MAX;
        } else if(minThumbPressed){
            result = Thumb.MIN;
        } else if(maxThumbPressed){
            result = Thumb.MAX;
        }
        return result;
    }

    private boolean isInThumbRange(float touchX, double normalizedThumbValue){
        float thumbPos = normalizedToScreen(normalizedThumbValue);
        float left = thumbPos - (getThumbWidth() / 2);
        float right = thumbPos + (getThumbWidth() / 2);
        float x = touchX - (getThumbWidth() / 2);
        if(thumbPos > (getWidth() - thumbWidth)) x = touchX;
        return (x >= left && x <= right);
    }

    private void onStartTrackingTouch(){
        mIsDragging = true;
    }

    private void onStopTrackingTouch(){
        mIsDragging = false;
    }

    private float normalizedToScreen(double normalizedCoord){
        float width = getWidth() - (barPadding * 2);
        return (float) normalizedCoord / 100f * width;
    }

    private double screenToNormalized(float screenCoord){
        double width = getWidth();

        if (width <= 2 * barPadding) {
            return 0d;
        } else {
            width = width - (barPadding * 2);
            double result = screenCoord / width * 100d;
            result = result - (barPadding / width * 100d);
            result = Math.min(100d, Math.max(0d, result));
            return result;

        }
    }

    private void setNormalizedMinValue(double value) {
        normalizedMinValue = Math.max(0d, Math.min(100d, Math.min(value, normalizedMaxValue)));
        if(fixGap != NO_FIXED_GAP && fixGap > 0){
            addFixGap(true);
        }
        else{
            addMinGap();
        }
        invalidate();
    }

    private void setNormalizedMaxValue(double value) {
        normalizedMaxValue = Math.max(0d, Math.min(100d, Math.max(value, normalizedMinValue)));
        if(fixGap != NO_FIXED_GAP && fixGap > 0){
            addFixGap(false);
        }
        else{
            addMaxGap();
        }
        invalidate();
    }

    private void addFixGap(boolean leftThumb){
        if(leftThumb){
            normalizedMaxValue = normalizedMinValue + fixGap;
            if(normalizedMaxValue >= 100){
                normalizedMaxValue = 100;
                normalizedMinValue = normalizedMaxValue - fixGap;
            }
        }
        else{
            normalizedMinValue = normalizedMaxValue - fixGap;
            if(normalizedMinValue <= 0){
                normalizedMinValue = 0;
                normalizedMaxValue = normalizedMinValue + fixGap;
            }
        }
    }

    private void addMinGap(){
        if((normalizedMinValue + gap) > normalizedMaxValue){
            double g = normalizedMinValue + gap;
            normalizedMaxValue = g;
            normalizedMaxValue = Math.max(0d, Math.min(100d, Math.max(g, normalizedMinValue)));
            if(normalizedMinValue >= (normalizedMaxValue - gap)){
                normalizedMinValue = normalizedMaxValue - gap;
            }
        }
    }

    private void addMaxGap(){
        if((normalizedMaxValue - gap) < normalizedMinValue){
            double g = normalizedMaxValue - gap;
            normalizedMinValue = g;
            normalizedMinValue = Math.max(0d, Math.min(100d, Math.min(g, normalizedMaxValue)));
            if(normalizedMaxValue <= (normalizedMinValue + gap)){
                normalizedMaxValue = normalizedMinValue + gap;
            }
        }
    }

    private double normalizedToValue(double normalized) {
        double val = normalized / 100 * (maxValue - minValue);
        val = val + minValue;
        return val;
    }

    private void attemptClaimDrag() {
        if (getParent() != null) {
            getParent().requestDisallowInterceptTouchEvent(true);
        }
    }

    private <T extends Number> Number formatValue(T value) throws IllegalArgumentException{
        final Double v = (Double) value;
        if (dataType == DataType.LONG) {
            return v.longValue();
        }
        if (dataType == DataType.DOUBLE) {
            return v;
        }
        if (dataType == DataType.INTEGER) {
            return Math.round(v);
        }
        if (dataType == DataType.FLOAT) {
            return v.floatValue();
        }
        if (dataType == DataType.SHORT) {
            return v.shortValue();
        }
        if (dataType == DataType.BYTE) {
            return v.byteValue();
        }
        throw new IllegalArgumentException("Number class '" + value.getClass().getName() + "' is not supported");
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(isInEditMode()) return;
        setupBar(canvas, paintF, rectF);
        setupHighlightBar(canvas, paintF ,rectF);
        setupLeftThumb(canvas, paintF, rectF);
        setupRightThumb(canvas, paintF, rectF);
    }

    @Override
    protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(getMeasureSpecWith(widthMeasureSpec), getMeasureSpecHeight(heightMeasureSpec));
    }

    @Override
    public synchronized boolean onTouchEvent(MotionEvent event) {

        if (!isEnabled())
            return false;

        final int action = event.getAction();

        switch (action & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                mActivePointerId = event.getPointerId(event.getPointerCount() - 1);
                int pointerIndex = event.findPointerIndex(mActivePointerId);
                float mDownMotionX = event.getX(pointerIndex);
                pressedThumb = evalPressedThumb(mDownMotionX);

                if(pressedThumb == null)
                    return super.onTouchEvent(event);

                setPressed(true);
                invalidate();
                onStartTrackingTouch();
                trackTouchEvent(event);
                attemptClaimDrag();
                break;
            case MotionEvent.ACTION_MOVE:
                if (pressedThumb != null) {
                    if (mIsDragging) {
                        trackTouchEvent(event);
                    }
                    if (onRangeSeekbarChangeListener != null) {
                        onRangeSeekbarChangeListener.valueChanged(this, getSelectedMinValue(), getSelectedMaxValue());
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                if (mIsDragging) {
                    trackTouchEvent(event);
                    onStopTrackingTouch();
                    setPressed(false);
                } else {
                    onStartTrackingTouch();
                    trackTouchEvent(event);
                    onStopTrackingTouch();
                }

                pressedThumb = null;
                invalidate();
                if (onRangeSeekbarChangeListener != null) {
                    onRangeSeekbarChangeListener.valueChanged(this, getSelectedMinValue(), getSelectedMaxValue());
                }
                break;
            case MotionEvent.ACTION_POINTER_DOWN: {
                break;
            }
            case MotionEvent.ACTION_POINTER_UP:
                invalidate();
                break;
            case MotionEvent.ACTION_CANCEL:
                if (mIsDragging) {
                    onStopTrackingTouch();
                    setPressed(false);
                }
                invalidate();
                break;
            default:
                break;
        }
        return true;
    }
}
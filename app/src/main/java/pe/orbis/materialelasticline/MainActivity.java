package pe.orbis.materialelasticline;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import pe.orbis.mylibrary.MaterialElasticLine;
import pe.orbis.mylibrary.OnRangeSeekbarChangeListener;

public class MainActivity extends AppCompatActivity implements OnRangeSeekbarChangeListener {

    private TextView lblMin;
    private TextView lblMax;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        MaterialElasticLine melDemo = (MaterialElasticLine) findViewById(R.id.melDemo);
        lblMin = (TextView) findViewById(R.id.lblMin);
        lblMax = (TextView) findViewById(R.id.lblMax);

        int minValue = 0;
        int maxValue = 100;

        melDemo.setCornerRadius(10);
        melDemo.setMinValue(minValue);
        melDemo.setMaxValue(maxValue);
        //melDemo.setThumbDiameter(getResources().getDimension(R.dimen.thumb_diameter));

        lblMin.setText(String.valueOf(minValue));
        lblMax.setText(String.valueOf(maxValue));

        melDemo.setOnRangeSeekbarChangeListener(this);
    }

    @Override
    public void valueChanged(View view, Number minValue, Number maxValue) {
        if(view.getId() == R.id.melDemo){
            lblMin.setText(String.valueOf(minValue));
            lblMax.setText(String.valueOf(maxValue));
        }
    }
}
